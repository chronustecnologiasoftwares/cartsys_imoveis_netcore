using System;
using System.Linq;
using Cartsys.Imoveis.Entity;
using Cartsys.Imoveis.Infrastructure;
using Cartsys.Imoveis.Repository.Interface;

namespace Cartsys.Imoveis.Repository
{
    public class PaisRepository : IPadraoRepository<Pais>, IDisposable
    {
        private CartsysContext _db = new CartsysContext();
        private IRepository<Pais> _repository;

        public PaisRepository(CartsysContext context = null)
        {
            _repository = new Repository<Pais>(context ?? new CartsysContext());
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public string Alterar(Pais entity)
        {
            string mensagem = ValidarDados(entity);
            if (mensagem == "")
                mensagem = _repository.Update(entity);
            return mensagem;
        }

        public string Excluir(Pais entity)
        {
            string mensagem = ValidarExclusao(entity);
            if (mensagem == "")
                mensagem = _repository.Delete(entity);
            return mensagem;
        }

        public IQueryable<Pais> Filtrar(string condicao)
        {
            return _repository.Filter(condicao);
        }

        public string Incluir(Pais entity)
        {
            string mensagem = ValidarDados(entity);
            if (mensagem == "")
                mensagem = _repository.Insert(entity);
            return mensagem;
        }

        public Pais Selecionar(int id)
        {
            return _repository.GetById(id);
        }

        public IQueryable<Pais> SelecionarTodos()
        {
            return _repository.GetAll();
        }

        public string ValidarDados(Pais entity)
        {
            if (string.IsNullOrWhiteSpace(entity.Codigo))
                return "Código não informado!";
            else if (string.IsNullOrWhiteSpace(entity.Nome))
                return "Nome não informado!";
            return "";
        }

        public string ValidarExclusao(Pais entity)
        {
            return "";
        }
    }
}