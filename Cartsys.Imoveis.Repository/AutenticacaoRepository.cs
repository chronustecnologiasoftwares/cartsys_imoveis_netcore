using System;
using System.Linq;
using Cartsys.Imoveis.Infrastructure;

namespace Cartsys.Imoveis.Repository
{
    public class AutenticacaoRepository: IDisposable
    {
        private CartsysContext _db = new CartsysContext();

        public AutenticacaoRepository()
        {
            
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public bool Check(string login, string senha)
        {
            return (from q in _db.Autenticacaos where q.Usuario == login && q.Senha == senha select q).Count() != 0;
        }
    }
}