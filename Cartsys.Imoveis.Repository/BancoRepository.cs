using System;
using System.Linq;
using Cartsys.Imoveis.Entity;
using Cartsys.Imoveis.Infrastructure;
using Cartsys.Imoveis.Repository.Interface;

namespace Cartsys.Imoveis.Repository
{
    public class BancoRepository : IPadraoRepository<Banco>, IDisposable
    {
        private CartsysContext _db = new CartsysContext();
        private IRepository<Banco> _repository;

        public BancoRepository(CartsysContext context = null)
        {
            _repository = new Repository<Banco>(context ?? new CartsysContext());
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public string Alterar(Banco entity)
        {
            string mensagem = ValidarDados(entity);
            if (mensagem == "")
                mensagem = _repository.Update(entity);
            return mensagem;
        }

        public string Excluir(Banco entity)
        {
            string mensagem = ValidarExclusao(entity);
            if (mensagem == "")
                mensagem = _repository.Delete(entity);
            return mensagem;
        }

        public IQueryable<Banco> Filtrar(string condicao)
        {
            return _repository.Filter(condicao);
        }

        public string Incluir(Banco entity)
        {
            string mensagem = ValidarDados(entity);
            if (mensagem == "")
                mensagem = _repository.Insert(entity);
            return mensagem;
        }

        public Banco Selecionar(int id)
        {
            return _repository.GetById(id);
        }

        public IQueryable<Banco> SelecionarTodos()
        {
            return _repository.GetAll();
        }

        public string ValidarDados(Banco entity)
        {
            if (string.IsNullOrWhiteSpace(entity.Nome))
                return "Nome não informado!";
            else if (string.IsNullOrWhiteSpace(entity.Numero))
                return "Número não informado!";
            return "";
        }

        public string ValidarExclusao(Banco entity)
        {
            return "";
        }
    }
}