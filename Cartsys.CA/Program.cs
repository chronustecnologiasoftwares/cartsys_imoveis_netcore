﻿using System;
using System.IO;

namespace Cartsys.CA // teste // teste
{
    class Program
    {
        static void Main(string[] args)
        {
            var nome = "Banco";
            var file = "";

            file = File.ReadAllText("../Template/_TEMPLATE_.cs");
            file = file.Replace("_TEMPLATE_", nome);
            File.WriteAllText($"../Cartsys.Imoveis.Entity/{nome}.cs", file);

            file = File.ReadAllText("../Template/_TEMPLATE_Mapping.cs");
            file = file.Replace("_TEMPLATE_", nome);
            File.WriteAllText($"../Cartsys.Imoveis.Infrastructure/Mapping/{nome}Mapping.cs", file);

            file = File.ReadAllText("../Template/_TEMPLATE_Repository.cs");
            file = file.Replace("_TEMPLATE_", nome);
            File.WriteAllText($"../Cartsys.Imoveis.Repository/{nome}Repository.cs", file);

            file = File.ReadAllText("../Template/_TEMPLATE_Controller.cs");
            file = file.Replace("_TEMPLATE_", nome);
            File.WriteAllText($"../Cartsys.Imoveis.Controller/Controllers/{nome}Controller.cs", file);
        }
    }
}
