using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Cartsys.Imoveis.Entity;
using Cartsys.Imoveis.Infrastructure;
using Cartsys.Imoveis.Repository;

namespace Cartsys.Imoveis.Controller.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/_TEMPLATE_")]
    public class _TEMPLATE_Controller : ControllerBase
    {
        private string _mensagem = "";

        [HttpPost("salvar")]
        public string Salvar([FromBody] _TEMPLATE_ entity)
        {
            using (var _db = new CartsysContext())
            {
                using (var transacao = _db.Database.BeginTransaction())
                {
                    if (entity.Id == 0)
                        _mensagem = new _TEMPLATE_Repository(_db).Incluir(entity);
                    else
                        _mensagem = new _TEMPLATE_Repository(_db).Alterar(entity);

                    if (_mensagem == "")
                        transacao.Commit();
                    else
                        transacao.Rollback();
                }
            }
            return _mensagem;
        }

        [HttpPost("excluir")]
        public string Excluir([FromBody] _TEMPLATE_ entity)
        {
            using (var _db = new CartsysContext())
            {
                using (var transacao = _db.Database.BeginTransaction())
                {
                    _mensagem = new _TEMPLATE_Repository(_db).Excluir(entity);

                    if (_mensagem == "")
                        transacao.Commit();
                    else
                        transacao.Rollback();
                }
            }
            return _mensagem;
        }

        [HttpGet("selecionar/{id}")]
        public _TEMPLATE_ Selecionar(int id)
        {
            return new _TEMPLATE_Repository().Selecionar(id);
        }

        [HttpGet("selecionartodos")]
        public List<_TEMPLATE_> SelecionarTodos()
        {
            return new _TEMPLATE_Repository().SelecionarTodos().ToList();
        }

        [HttpPost("filtrar")]
        public List<_TEMPLATE_> Filtrar([FromBody] string condicao)
        {
            return new _TEMPLATE_Repository().Filtrar(condicao).ToList();
        }
    }
}