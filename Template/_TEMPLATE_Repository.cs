using System;
using System.Linq;
using Cartsys.Imoveis.Entity;
using Cartsys.Imoveis.Infrastructure;
using Cartsys.Imoveis.Repository.Interface;

namespace Cartsys.Imoveis.Repository
{
    public class _TEMPLATE_Repository : IPadraoRepository<_TEMPLATE_>, IDisposable
    {
        private CartsysContext _db = new CartsysContext();
        private IRepository<_TEMPLATE_> _repository;

        public _TEMPLATE_Repository(CartsysContext context = null)
        {
            _repository = new Repository<_TEMPLATE_>(context ?? new CartsysContext());
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public string Alterar(_TEMPLATE_ entity)
        {
            string mensagem = ValidarDados(entity);
            if (mensagem == "")
                mensagem = _repository.Update(entity);
            return mensagem;
        }

        public string Excluir(_TEMPLATE_ entity)
        {
            string mensagem = ValidarExclusao(entity);
            if (mensagem == "")
                mensagem = _repository.Delete(entity);
            return mensagem;
        }

        public IQueryable<_TEMPLATE_> Filtrar(string condicao)
        {
            return _repository.Filter(condicao);
        }

        public string Incluir(_TEMPLATE_ entity)
        {
            string mensagem = ValidarDados(entity);
            if (mensagem == "")
                mensagem = _repository.Insert(entity);
            return mensagem;
        }

        public _TEMPLATE_ Selecionar(int id)
        {
            return _repository.GetById(id);
        }

        public IQueryable<_TEMPLATE_> SelecionarTodos()
        {
            return _repository.GetAll();
        }

        public string ValidarDados(_TEMPLATE_ entity)
        {
            return "";
        }

        public string ValidarExclusao(_TEMPLATE_ entity)
        {
            return "";
        }
    }
}