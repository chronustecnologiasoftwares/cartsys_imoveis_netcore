using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Cartsys.Imoveis.Entity;

namespace Workflow.Infrastructure.Mapping
{
    internal class _TEMPLATE_Mapping : IEntityTypeConfiguration<_TEMPLATE_>
    {
        public void Configure(EntityTypeBuilder<_TEMPLATE_> builder)
        {
            builder.ToTable("_TEMPLATE_");
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Id).HasColumnName("ID").ValueGeneratedOnAdd();
        }
    }
}