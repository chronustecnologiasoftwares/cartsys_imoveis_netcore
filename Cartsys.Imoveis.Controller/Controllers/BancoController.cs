using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Cartsys.Imoveis.Entity;
using Cartsys.Imoveis.Infrastructure;
using Cartsys.Imoveis.Repository;

namespace Cartsys.Imoveis.Controller.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/Banco")]
    public class BancoController : ControllerBase
    {
        private string _mensagem = "";

        [HttpPost("salvar")]
        public string Salvar([FromBody] Banco entity)
        {
            using (var _db = new CartsysContext())
            {
                using (var transacao = _db.Database.BeginTransaction())
                {
                    if (entity.Id == 0)
                        _mensagem = new BancoRepository(_db).Incluir(entity);
                    else
                        _mensagem = new BancoRepository(_db).Alterar(entity);

                    if (_mensagem == "")
                        transacao.Commit();
                    else
                        transacao.Rollback();
                }
            }
            return _mensagem;
        }

        [HttpPost("excluir")]
        public string Excluir([FromBody] Banco entity)
        {
            using (var _db = new CartsysContext())
            {
                using (var transacao = _db.Database.BeginTransaction())
                {
                    _mensagem = new BancoRepository(_db).Excluir(entity);

                    if (_mensagem == "")
                        transacao.Commit();
                    else
                        transacao.Rollback();
                }
            }
            return _mensagem;
        }

        [HttpGet("selecionar/{id}")]
        public Banco Selecionar(int id)
        {
            return new BancoRepository().Selecionar(id);
        }

        [HttpGet("selecionartodos")]
        public List<Banco> SelecionarTodos()
        {
            return new BancoRepository().SelecionarTodos().ToList();
        }

        [HttpPost("filtrar")]
        public List<Banco> Filtrar([FromBody] string condicao)
        {
            return new BancoRepository().Filtrar(condicao).ToList();
        }
    }
}