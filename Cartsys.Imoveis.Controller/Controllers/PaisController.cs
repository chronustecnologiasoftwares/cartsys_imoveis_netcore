using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Cartsys.Imoveis.Entity;
using Cartsys.Imoveis.Infrastructure;
using Cartsys.Imoveis.Repository;
using Cartsys.Imoveis.DTO;

namespace Cartsys.Imoveis.Controller.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/Pais")]
    public class PaisController : ControllerBase
    {
        private string _mensagem = "";

        [HttpPost("salvar")]
        public string Salvar([FromBody] Pais entity)
        {
            using (var _db = new CartsysContext())
            {
                using (var transacao = _db.Database.BeginTransaction())
                {
                    if (entity.Id == 0)
                        _mensagem = new PaisRepository(_db).Incluir(entity);
                    else
                        _mensagem = new PaisRepository(_db).Alterar(entity);

                    if (_mensagem == "")
                        transacao.Commit();
                    else
                        transacao.Rollback();
                }
            }
            return _mensagem;
        }

        [HttpPost("excluir")]
        public string Excluir([FromBody] Pais entity)
        {
            using (var _db = new CartsysContext())
            {
                using (var transacao = _db.Database.BeginTransaction())
                {
                    _mensagem = new PaisRepository(_db).Excluir(entity);

                    if (_mensagem == "")
                        transacao.Commit();
                    else
                        transacao.Rollback();
                }
            }
            return _mensagem;
        }

        [HttpGet("selecionar/{id}")]
        public Pais Selecionar(int id)
        {
            return new PaisRepository().Selecionar(id);
        }

        [HttpGet("selecionartodos")]
        public List<Pais> SelecionarTodos()
        {
            return new PaisRepository().SelecionarTodos().ToList();
        }

        [HttpPost("filtrar")]
        public List<Pais> Filtrar([FromBody] FiltroDTO condicao)
        {
            return new PaisRepository().Filtrar(condicao.Condicao).ToList();
        }
    }
}