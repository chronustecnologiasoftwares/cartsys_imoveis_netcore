﻿using System.Threading.Tasks;
using Cartsys.Imoveis.Repository;

namespace Cartsys.Imoveis.Controller
{
    public interface IAuthenticationService
    {
        Task<bool> Authenticate(string username, string password);
    }

    internal class AuthenticationService : IAuthenticationService
    {
        public async Task<bool> Authenticate(string username, string password)
        {
            var success = await Task.Run(() => new AutenticacaoRepository().Check(username, password));
            return success;
        }
    }
}
