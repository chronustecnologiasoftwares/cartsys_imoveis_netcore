using Cartsys.Imoveis.Entity;
using FirebirdSql.EntityFrameworkCore.Firebird.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using System.Text;
using Workflow.Infrastructure.Mapping;

namespace Cartsys.Imoveis.Infrastructure
{
    public class CartsysContext: DbContext
    {
        public CartsysContext()
        {
            if (!Database.GetService<IRelationalDatabaseCreator>().Exists())
            {
                Database.EnsureCreated();
                CartsysInitialization.Seed(this);
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            var provider = configuration.GetSection("AppSettings").GetValue<string>("DatabaseProvider");
            if (provider.Equals("Firebird"))
                optionsBuilder.UseFirebird(configuration.GetConnectionString("CartsysImoveisConnectionString"));
            else if (provider.Equals("SqlServer"))
                optionsBuilder.UseSqlServer(configuration.GetConnectionString("CartsysImoveisConnectionString"));

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AutenticacaoMapping());
            modelBuilder.ApplyConfiguration(new PaisMapping());
            modelBuilder.ApplyConfiguration(new BancoMapping());
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Autenticacao> Autenticacaos { get; set;}
        public DbSet<Banco> Bancos { get; set; }
        public DbSet<Pais> Paises { get; set; }
    }
}