using Cartsys.Imoveis.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Cartsys.Imoveis.Infrastructure
{
    internal class CartsysInitialization
    {
        public static void Seed(CartsysContext context)
        {
            if (context.Autenticacaos.Local.Count == 0)
            {
                context.Autenticacaos.Add(new Autenticacao { Id = 1, Usuario = "usuario", Senha = "senha"} );
                context.SaveChanges();
            }

            var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            var provider = configuration.GetSection("AppSettings").GetValue<string>("DatabaseProvider");
            if (provider.Equals("Firebird"))
            {
                CreateSequences(context);
                CreateTriggersBeforeInsert(context);
            }
        }

        private static void CreateSequences(CartsysContext context)
        {
            context.Database.ExecuteSqlCommand("CREATE SEQUENCE GEN_PAISES_ID;");
            context.Database.ExecuteSqlCommand("CREATE SEQUENCE GEN_BANCO_ID");
        }

        private static void CreateTriggersBeforeInsert(CartsysContext context)
        {
            context.Database.ExecuteSqlCommand("CREATE OR ALTER trigger paises_bi for paises\r\nactive before insert position 0\r\nAS\r\nBEGIN\r\n  NEW.ID_PAIS = GEN_ID(GEN_PAISES_ID,1);\r\nEND");
            context.Database.ExecuteSqlCommand("CREATE OR ALTER trigger banco_bi for banco\r\nactive before insert position 0\r\nAS\r\nBEGIN\r\n  NEW.ID_BANCO = GEN_ID(GEN_BANCO_ID,1);\r\nEND");
        }
    }
}