using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Cartsys.Imoveis.Entity;

namespace Workflow.Infrastructure.Mapping
{
    internal class AutenticacaoMapping : IEntityTypeConfiguration<Autenticacao>
    {
        public void Configure(EntityTypeBuilder<Autenticacao> builder)
        {
            builder.ToTable("TBL_AUTENTICACAO");
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Id).HasColumnName("ID").ValueGeneratedNever();
            builder.Property(p => p.Usuario).HasColumnName("USUARIO").HasMaxLength(50).IsRequired();
            builder.Property(p => p.Senha).HasColumnName("SENHA").HasMaxLength(50).IsRequired();
        }
    }
}