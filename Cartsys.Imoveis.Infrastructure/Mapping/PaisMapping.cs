using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Cartsys.Imoveis.Entity;

namespace Workflow.Infrastructure.Mapping
{
    internal class PaisMapping : IEntityTypeConfiguration<Pais>
    {
        public void Configure(EntityTypeBuilder<Pais> builder)
        {
            builder.ToTable("PAISES");
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Id).HasColumnName("ID_PAIS").ValueGeneratedOnAdd();
            builder.Property(p => p.Codigo).HasColumnName("CODIGO_PAIS").HasMaxLength(10).IsRequired();
            builder.Property(p => p.Nome).HasColumnName("PAIS").HasMaxLength(50).IsRequired();
        }
    }
}