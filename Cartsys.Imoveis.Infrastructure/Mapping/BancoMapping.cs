using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Cartsys.Imoveis.Entity;

namespace Workflow.Infrastructure.Mapping
{
    internal class BancoMapping : IEntityTypeConfiguration<Banco>
    {
        public void Configure(EntityTypeBuilder<Banco> builder)
        {
            builder.ToTable("BANCO");
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Id).HasColumnName("ID_BANCO").ValueGeneratedOnAdd();
            builder.Property(p => p.Nome).HasColumnName("NOME").HasMaxLength(80).IsRequired();
            builder.Property(p => p.Numero).HasColumnName("NUMERO").HasMaxLength(10).IsRequired();
        }
    }
}