namespace Cartsys.Imoveis.DTO
{
    public class FiltroDTO
    {
        public string Campo { get; set; }
        public string Condicao { get; set; }
    }
}