namespace Cartsys.Imoveis.Entity
{
    public class Pais : BaseClass
    {
        public string Codigo { get; set; }

        public string Nome { get; set; }
    }
}